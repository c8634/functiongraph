#include <stdio.h>
#include <stdlib.h>
#include <string.h>                     //memset, memcpy, strlen, strcpy
#include <unistd.h>                     //usleep
#include <math.h>                       //roundf
#include "tinyexpr_modified/tinyexpr.h"

#define exprmax 64
#define buffmax 1000000

/*
 * Program that plots a function graph in terminal with a given function
 *
 * The source code contains a lot of comments descriptions and details about
 * optimizations.
 *
 * Matrix info: the matrix is a reference to a dynamic memory area (array) of
 * yaxis*(xaxis+1) size, we refer to lines as sub-areas with xaxis length.
 *
 * Why xaxis+1?, this is done to contain \n at the end of every line and
 * simplify the print process.
 *
 * What is the temporal variable?, it stores the time that has passed since
 * start, it can be used as t, to animate functions in temporal mode.
 *
 * Error codes: 
 * 	1, bad arguments
 * 	2, bad memory allocation
 */

// resizes the values of the y and x axis of the matrix, 
// based on the screen size
static inline void auto_resize( int* pxaxis, int* pyaxis )
{

	// reads from a stream with the command stdout 
	// (the command outputs terminal sizes in the structure lines\ncols)
	FILE *fp = popen("tput lines && tput cols", "r");

	fscanf(fp, "%d", pyaxis);
	fscanf(fp, "%d", pxaxis);

	pclose(fp);

	// the last "column" is needed for newline characters
	*pxaxis-=1;
	// the screen should not scroll (with many prints per second it will produce
	// bad artifacts), with ANSI sequences the program uses the same terminal
	// screen area without scrolling.
	// When the screen lines are odd a print operation will scroll the screen
	// by 1 line, that will offset the cursor home position by 1, 
	// and so on at every print.
	*pyaxis-=1;

	// makes the axes odd, they have to symmetrically divide the matrix
	// (one column and row will be used for actual axes lines)
	if (((*pxaxis % 2) == 0))
		*pxaxis -= 1;
	if ((*pyaxis % 2 == 0))
		*pyaxis -= 1;
}

// fills the matrix with blank characters and puts the y and x axis characters
static inline void reset_graph( int yaxis, int xaxis, char* matrix )
{
	// fills the first xaxis adresses (line) with blank characters, 
	// (memset counts from 1)
	memset(matrix, ' ',xaxis);
	// fills the middle-line with '|'
	memset(&matrix[xaxis>>1],'|',1);
	// fills the last (xaxis+1) address with a newline (array is 0-xaxis long)
	memset(&matrix[xaxis],'\n',1);

	// for every line
	for (int y = 1; y < yaxis; y++)
	{
		// &matrix[y*(xaxis+1)] is the address of the first column of 
		// the "y" line, with this we copy the first matrix line 
		// (matrix[0]) to the "y" remaining
		memcpy(&matrix[y*(xaxis+1)], matrix, xaxis+1);
	}

	// it fills the middle-line with '-',
	// optimization=this saves up 2800 instructions compared to the for cycle
	memset(&matrix[(yaxis>>1)*(xaxis+1)], '-', xaxis);
}

// calculates the function and fills the matrix
static inline void calculate( int yaxis, int xaxis, char* matrix,
                              float zoom, uint temp, char* expr )
{
	// it offsets the starting point from the last matrix 
	// value matrix[maxy][maxx] to the center matrix[halfy][halfx], 
	// so we can introduce negative values
	yaxis>>=1;
	xaxis>>=1;

	// temporal variable is calculated, (temp contains how much ms have passed)
	float t= (float)temp/1000;
	float xz;

	te_variable vars[2] = {{"x", &xz}, {"t", &t}};
	te_expr *tempexpr = te_compile(expr, vars, 2, 0);

	// for every x starting from -xaxis
	for (int x = -xaxis; x <= xaxis; x++)
	{
		// a sub-value (xz) is made for zooming, 
		// so whenever x reaches a zoom multiple,
		// xz increases by one, also it is divided by 2,
		// because terminal cells have about 1/2 width length ratio
		xz = (float)x/(zoom*2);
		// a y subvalue (yz) is calculated, here goes the function, yz=f(xz)
		float yz = te_eval(tempexpr);

		// yz is zoomed, rounded by the nearest integer and put in y
		int y = roundf(yz*zoom);

		// if y isn't bigger than the y axis
		if (y <= yaxis && y >= -yaxis)
		{
			// with this ((xaxis+1)<<1)*(yaxis-y)+xaxis+x, 
			// it translates from cartesian coordinates to our dynamic 
			// array (matrix) elements positions, remember that in this
			// function xaxis and yaxis are halved
			matrix[((xaxis+1)<<1)*(yaxis-y)+xaxis+x] = '*';
		}
	}
	te_free(tempexpr);
}

// prints the "matrix" in stdout
static inline void print_graph( int yaxis, int xaxis, char* matrix )
{
	// every byte of the "matrix" is printed in stdout,
	// optimization, write saves about 20000 instructions compared to 
	// fwrite that saves about between 1000-3000 instructions
	// compared to fputs, puts, printf.
	//
	// stdout buffer is not needed, it should be disabled, buffering and thus
	// even buffered functions like fwrite add only overhead, because there's
	// the need to print the entire matrix at every write(...) call.
	write(1, matrix, (xaxis+1)*yaxis);
}

// pauses the thread for ms milliseconds
static inline void delay( unsigned int ms )
{
	// usleep uses us, ("multiplied" by 1024 for performance, 
	// we don't need high precision and accuracy)
	usleep(ms<<10);
}

static inline int args_handler( int argc, char** argv, float *zoom,
                                int *cycle_ms, char *expr )
{
	int index,option;
	float buff;

	while ((option = getopt(argc, argv, ":m:z:f:h")) != -1)
	{
		switch (option)
		{
			case 'm':
			{
				buff = atoi(optarg);
				if (buff > 15 && buff < buffmax)
				{
					*cycle_ms = buff;
				}
				else
				{
					optopt=option;
					goto noarg;
				}
				break;
			}

			case 'z':
			{
				buff = atof(optarg);
				if (buff > 0 && buff < buffmax)
				{
					*zoom = buff;
				}
				else
				{
					optopt=option;
					goto noarg;
				}
				break;
			}

			case 'f':
			{
				if (strlen(optarg)<exprmax) 
				{
					strcpy(expr,optarg);
				}
				else
				{
					optopt=option;
					goto noarg;
				}
				break;
			}

			case 'h': 
			{
				puts( "-h  | shows this help\n"
				      "-z  | zoom level\n"
				      "	default: 1\n"
				      "-m  | refresh rate in ms\n"
				      "	default: 0, if >0 activates temporal mode\n"
				      "-f  | function expression\n"
				      "	default: \"\" \n\n"
				      "example: \n"
				      "	./functiongraph -f \"sin(x+t)\" -z 5 -m 32"
				    );
				return 1;
			}

			case ':':
			{
				noarg:
				fprintf( stderr, "option -%c requires a valid argument \n", 
				         optopt );
				return 1;
			}

			case '?':
			{
				fprintf(stderr, "unknown option -%c, -h for help \n", optopt);
				return 1;
			}

			default:
			{
				return 1;
			}
		}
	}

	if(optind < argc)
	{
		printf("arguments require options, -h for help \n");
		return 1;
	}

	return 0;
}


int main( int argc, char** argv )
{
	/* Variables */

	// zoom, 1/zoom= value of 1 bar
	float zoom= 1;

	// refresh rate, if 0 temporal mode is disabled
	int cycle_ms= 0;

	char expr[exprmax];

	if (args_handler(argc,argv,&zoom,&cycle_ms,expr))
		return 1;

	// temporal variable
	uint temp = 0;

	// declares the axes
	int xaxis = 0, yaxis = 0;
	// resizes them
	auto_resize(&xaxis, &yaxis);

	// matrix
	char* matrix=malloc((xaxis+1)*yaxis);
	if (matrix == NULL) {
		fprintf(stderr, "matrix allocation failed\n");
		return 2;
	}

	/* makes the graph */
	
	//disables stdout buffering, see print_graph(...) for more info
	setvbuf(stdout, NULL, _IONBF, 0);

	// ANSI squence that puts terminal cursor to position 0
	fputs("\033[H",stdout);

	do
	{
		// it refreshes the matrix size every 500ms,
		// when temp (that is a multiple of cycle_ms) is not multiple of 500
		// the module will never be 0 (the module is between 0 and cycle_ms)
		if (temp % 500 < cycle_ms) 
		{
			int x=xaxis;
			int y=yaxis;
			auto_resize(&xaxis, &yaxis);
			//avoids reallocation when nothing changes
			if(x!=xaxis || y!=yaxis)
			{
				matrix=realloc(matrix,(xaxis+1)*yaxis);
				if (matrix == NULL)
				{
					fprintf(stderr, "matrix allocation failed\n");
					return 2;
				}
			}
		}

		reset_graph(yaxis, xaxis, matrix);
		calculate(yaxis, xaxis, matrix, zoom, temp, expr);
		print_graph(yaxis, xaxis, matrix);

		delay(cycle_ms);
		temp += cycle_ms;

		fputs("\033[H",stdout);

	//while the temporal mode is on
	} while (cycle_ms != 0);

	free(matrix);

	return 0;
}
