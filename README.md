<div align="center">
  <img width="600" src="https://i.imgur.com/f5yUzDh.gif">
</div>


## Index

* [Description](#Description)
* [Installation](#installation)
* [Usage](#usage)
* [License](#license)
* [Acknowledgements](#acknowledgements)


## Description

Program that plots a function graph in terminal, with support of animation.


### Main Goal
* Efficiency

The source code contains a lot of comments, descriptions, and details.\
[Valgrind cachegrind](https://valgrind.org/docs/manual/cg-manual.html) has been used to benchmark and helped to reduce the total instructions number.\
[Tinyexpr library](https://github.com/codeplea/tinyexpr), has been used to evaluate math expressions arguments, also it has been modified to float types only.

## Installation

1. Clone the repo
```sh
git clone https://gitlab.com/c8634/functiongraph.git
```
2. build
```sh
make
```

## Usage

```sh
./functiongraph -h
```


### Functions examples
```sh
1) ./functiongraph -f "sqrt(x * (-x + 2))" -z 15
2) ./functiongraph -f "(t / (x * pi))*sin(pow(x, 2))" -z 8 -m 32
3) ./functiongraph -f "(10 / (x * pi))*sin(pow(x, 2))" -z 8
4) ./functiongraph -f "pow(x, x * t)" -z 8 -m 32
5) ./FunctionGraph -f "sin(x+t)" -z 5 -m 32
6) ./functiongraph -f "sin(x * 5 * sin(t))" -z 8 -m 32
```


## License

Distributed under the ```BSD 2-Clause "Simplified"``` license.

## Acknowledgements

* [ANSI Escape Sequences](https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797)
* [Valgrind cachegrind docs](https://valgrind.org/docs/manual/cg-manual.html)
* [Tinyexpr library on github](https://github.com/codeplea/tinyexpr)
* [Tinyexpr library on codeplea](https://codeplea.com/tinyexpr)
