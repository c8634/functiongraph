CC = gcc
CCFLAGS = -lm -funroll-loops -finline-functions -Ofast -fsingle-precision-constant


functiongraph: main.c tinyexpr_modified/tinyexpr.c
	$(CC) -o functiongraph main.c tinyexpr_modified/tinyexpr.c $(CCFLAGS)

clean:
	rm functiongraph
